<?php
/**
 * PDO DATABASE CLASS INSTANTIATION
 * DATABASE CONNECTION
 * CREATING PREPARED STATEMENTS
 * BINDING VALUES
 * RETURNING ROWS AND RESULTS
 * 
 */

class Database {
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;
    private $dbh;
    private $error;
    private $stmt;
    public function __construct(){
        //set DSN
        $dsn = 'mysql:host='.$this->host.';dbname='.$this->dbname;
        $options =array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE =>PDO::ERRMODE_EXCEPTION
        );
        // Create Pdo instance
        try{
            // database handler
            $this->dbh = new PDO($dsn,$this->user,$this->pass,$options);

        }catch(PDOException $e){
            $this->error = $e->getMessage();
            echo $this->error;
        }
    }
    // Prepare statements with query
    
    public function query($sql){
        $this->stmt = $this->dbh->prepare($sql);
    }
    // Bind values to statements
    public function bind($param,$value,$type=null){
        if(is_null($type)){
             switch (true){
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;                
            }
                
        }
        $this->stmt->bindValue($param,$value,$type);
    }

    // Execute the prepared statements
    public function execute(){
        return $this->stmt->execute();
    }
    //Get result set as arrays of objects
    public function resultSet(){
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }
    // Get single record as object
    public function single(){
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }
    // Get number of records(Row Count)
    
    public function rowCount(){
        return $this->$stmt->rowCount();
    }




}
 
 