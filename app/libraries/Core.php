<?php 
/**
 * This App core class
 * Creates
 * Url format controller/method/params
 * 
 */

 class Core {
     protected $currentController = 'Pages';
     protected $currentMethod = 'index';
     protected $params = [];

     public function __construct(){
        //  print_r($this->getUrl());
        $url = $this->getUrl();
        // look for controller class in controllers and set it to first url array
        if(file_exists('../app/controllers/'.ucwords($url[0]).'.php')){
            // if file exist,set it as first array value
            $this->currentController = ucwords($url[0]);
            unset($url[0]);
        }
        // Require the controller

        require_once '../app/controllers/'.$this->currentController .'.php';
        // instantiate the class

        $this->currentController = new $this->currentController ;

        // Checking for the second array index (i.e method ) in controller 
        if(isset($url[1])){
            if(method_exists($this->currentController,$url[1])){
                $this->currentMethod = $url[1];
                unset($url[1]);

            }

        }
        // echo '<br>'.$this->currentMethod;
        // Getting Param(i.e third array value)

        $this->params = $url ? array_values($url):[];
        call_user_func_array([$this->currentController,$this->currentMethod],$this->params);

     }

     public function getUrl(){
         if(isset($_GET['url'])){
            $url = rtrim($_GET['url'],'/');
            $url = filter_var($url,FILTER_SANITIZE_URL);  
            $url = explode('/',$url);
            return $url;
         }
     }
 }