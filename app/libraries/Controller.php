<?php
/**
 * Base Controller
 * Loads  models and views
 * 
 * 
 * 
 * */ 
    class Controller {
        // loads model
        public function model($model){
            // Require the model file
            require_once '../app/models/'.$model.'.php';
            // Instantiate a new model from the class
            return new $model();
        }
        
        // Loads view
        public function view($view, $data =[]){
            if(file_exists('../app/views/'.$view.'.php')){
                require_once '../app/views/'.$view.'.php';
            }else{
                die('The view '.$view.' does not exist');
            }
        }
    }